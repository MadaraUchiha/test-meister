# Test Meister #
Smart test distributor for driving instructors and schools.

## The Problem ##
A driving school employs multiple driving instructors (or teachers).

Every month, the school gets two reports:

 1. How many tests each teacher gets from the government this month (based on how many students that teacher has)
 2. The allocation of tests to the school, that is, at which days/times the school has how many tests.
    (for example, 2015-03-30T12:30 the school has 4 students under tests)
    
In addition, each teacher may submit requests (requests are submitted physically to the operator, and operators inputs
them into the program). A teacher can request a test at a specific date, and also request that no tests will happen
on a specific date.
    
Test Meister is to take those two reports, plus the teacher's requests as input, and output a complete test plan
for that month, indicating which teacher has tests at which times.

## Extra details ##
The times of tests are always deterministic: 7:30, 8:40, 9:50, 11:00, 12:30, 13:40, 14:50.

 - Tests are not performed by teachers, but by government employees.
 - A "test" consists of two students being under test. The reports' units are in students (as in, taking the example
   from section 2. above, that counts as 2 tests, total of 4 students.
 - Tests *always* come in pairs. A teacher can never send 1, 3, 5 etc students a month. Always an odd number.
 - A "test" belonging to a teacher means that both students under test belong to that same teacher.
 - In all times except for 7:30 and 14:50, a test may be *split* in to a test with 2 students from 2 teachers.
 - A teacher who got his test split, must have another test split. As said above, teachers always send an even number
   of students to tests.
 
## Restrictions ##

 - 7:30, being early in the morning, is a time no one wants. However other times are distributed, the distribution
   to 7:30 must be fair (teacher who got 7:30 this month won't get it again next month unless necessary).
 - Two tests by the same teacher should not happen on the same day. (Except if there are more tests than days, in which
   case there's no choice). In the case there's no choice, tests must not occur in consecutive times.
 - A teacher whose request got denied (3 teachers asking for the same day, when there's only 2 tests available there)
   will get priority in the next request.
// Created by madara all rights reserved.

'use strict';

let gulp = require('gulp');
let jshint = require('gulp-jshint');
let stylish = require('jshint-stylish');
let mocha = require('gulp-mocha');
let less = require('gulp-less');
let sourcemaps = require('gulp-sourcemaps');
let del = require('delete');

const scriptFiles = ['src/**/*.js', 'test/**/*.js', '*.js'];
const mainLessFile = 'src/less/main.less';
const lessFiles = ['src/less/*.less'];

gulp.task('lint', function lintAllFiles() {
    return gulp.src(scriptFiles)
        .pipe(jshint({
            esnext: true,
            node: true
        }))
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'));
});

gulp.task('less', function convertLessToCSS() {
    return gulp.src(mainLessFile)
        .pipe(sourcemaps.init())
        .pipe(less({
            paths: [lessFiles],
            compress: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css/'));
});

gulp.task('clean', function cleanDist() {
    del.sync('dist', {force: true});
});

gulp.task('watch', ['clean', 'lint', 'less'], function watchForChanges() {
    gulp.watch(scriptFiles, ['lint']);
    gulp.watch(lessFiles, ['less']);
});

gulp.task('default', ['lint', 'less']);
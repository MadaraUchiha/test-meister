// Created by madara all rights reserved.

import express from 'express';

import hogan from 'hogan-express';
import compress from 'compression';

// Routes are like controllers!
import index from './routes/index';

let app = express();

// Set the templating engine to hogan.
app.engine('html', hogan);
app.set('view engine', 'html');
app.set('views', './src/templates');

app.use('/assets', compress());
app.use('/assets', express.static(`${__dirname}/../dist`));
app.get('/', index);

app.listen(8080, () => console.log('Server is running!'));
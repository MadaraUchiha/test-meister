// Created by madara all rights reserved.

import {Router} from 'express';

let indexRouter = Router();

indexRouter.get('/', (request, response) => response.render('index'));

export default indexRouter;